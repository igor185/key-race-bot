const raceRoom = require('./raceRoom');
const maxAmountOfUsers = 5;
const minAmountOfUsers = 1;
const timeInPreRace = 15000; //ms
let timer;
const send = (socket, path, params) => {
    socket.broadcast.emit(path, {...params});
    socket.emit(path, {...params});
};

const timeToRace = (socket, usersWait, default_time) => {
    let left_time = default_time;
    return setInterval(() => {
        // if all users went out
        if (usersWait.usersWait.size === 0) {
            left_time = default_time;
            return;
        }
        // init race
        if (left_time <= 1000) {
            if (usersWait.usersWait.size >= minAmountOfUsers) {
                beginRace(socket, usersWait);
                return;
            }
            left_time = default_time + 1000;
        }
        left_time -= 1000;
        send(socket, 'time_preRace', {time: left_time, message: "Race in will begin when there are 5 users here or through "});
    }, 1000);
};

const beginRace = (socket, usersWait) => {
    const raceNumber = Math.round(Math.random() * 100000 + 1000);
    const users = [...usersWait.usersWait];
    usersWait.usersWait.clear();

    send(socket, 'nearlyStart', {raceNumber, users})
};


module.exports = (socket, user, usersWait) => {
    if (!timer)
        timer = timeToRace(socket.to('waitRoom'), usersWait,timeInPreRace);

    if (usersWait.usersWait.size === maxAmountOfUsers)
        beginRace(socket.to('waitRoom'), usersWait);

    socket.on('join_race', ({room, users}) => {
        clearInterval(timer);
        timer = undefined;
        socket.leave('waitRoom');
        socket.join(room);
        socket.emit('join_race');
        raceRoom(socket, room, user, users);
    });

    socket.on('disconnect', () => {
        usersWait.usersWait.delete(user.login);
        socket.broadcast.emit('leaveUser', {user: user.login});
    });

};
