const timeToStart = 15000;
const timeForRace = 60000;
const texts = require('../data/texts');

let Factory = require('../bot/Factory');
Factory = new Factory();

const timers = new Map();
const timersRace = new Map();

const Bots = new Map();
const _ = require('lodash');

const sendWithSocket = (socket, raceNumber, path, params) => {
    socket.broadcast.emit(path, {raceNumber, ...params});
    socket.emit(path, {raceNumber,...params});
};

const timeRace = _.curry((send, bot) => {
    bot.startRace();
    let left_time = timeForRace;
    return setInterval(() => {
        if (left_time === 0)
            return;
        if (left_time <= 1000) {
            bot.finishRace();
            send('finish', {time: left_time})
            send('end_time', {default_time: timeForRace});
            send('endRace', {});
            left_time = 0;
            return;
        }
        left_time -= 1000;
        if ((timeForRace - left_time) > 0 && (timeForRace - left_time) % 30000 === 0)
            bot.sendStatistic();
        else if ((timeForRace - left_time) > 0 && (timeForRace - left_time) % 15000 === 0) {
            bot.sendComment();
        }
        send('time_race', {time: left_time, message: 'time to finish', timeForRace})
    }, 1000);
});

const timeToRaceStart = (send, raceNumber, bot) => {
    let left_time = timeToStart;

    return setInterval(() => {
        if (left_time === 0) {
            clearInterval(timers.get(raceNumber));
            timers.delete(raceNumber);
            return;
        }
        if (left_time <= 1000) {
            send('start', {time: left_time})
            left_time = 0;
            const timer = timeRace(send)(bot);
            timersRace.set(raceNumber, timer);
            return;
        }

        send('time_preStart', {time: left_time, message: 'time to start'})
        if (left_time === timeToStart)
            bot.initRace();
        left_time -= 1000;
    }, 1000);
};

module.exports = (socket, raceNumber, user, users) => {

    let send = _.partial(sendWithSocket, socket.to(raceNumber), raceNumber);
    let amountUsers = users.length;

    const sizeText = texts[raceNumber % texts.length].length;

    if (!Bots.has(raceNumber))
        Bots.set(raceNumber, Factory.create(send, users, sizeText, raceNumber));
    const bot = Bots.get(raceNumber);

    if (!timers.has(raceNumber)) {
        const timer = timeToRaceStart(send, raceNumber, bot);
        timers.set(raceNumber, timer);
    }

    socket.on('progress', (data) => {
        sendWithSocket(socket.to(data.raceNumber), data.raceNumber, 'progress', {...data, user: user.login});
        Bots.get(data.raceNumber).setStatistic({...data, user: user.login});
    });
    socket.on('disconnect', () => {
        socket.to(raceNumber).broadcast.emit('leaveUser', {username: user.login});
    });
    socket.on('userEndRace', (data) => {
        send('userEndRace', data);
        amountUsers--;
        if (amountUsers === 0) {
            bot.finishRace();
            send('endRace', {});
            clearInterval(timersRace.get(raceNumber));
            timersRace.delete(raceNumber);
        }
    });
};
