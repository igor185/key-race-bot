const jwt = require('jsonwebtoken');

const preRaceRoom = require('./preRaceRoom');

const setHandlers = (socket, usersWait) => {
    let user;
    try {
        user = jwt.verify(socket.handshake.query.token, 'secretWord');

        if (!user)
            return;

        usersWait.usersWait.add(user.login);
        socket.join('waitRoom');
        socket.to('waitRoom').broadcast.emit('joinUser',{user:user.login});

        preRaceRoom(socket,user, usersWait);

        socket.on('join',({room})=>{
            socket.join(room);
            socket.to(room).emit('join',{room});
        });
    }catch (e) {
        socket.emit('delete_jwt');
    }
};

module.exports = setHandlers;