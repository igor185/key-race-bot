let timeNow = 0;
let race;
class Race {
    constructor(root, socket, users, raceNumber) {
        socket.removeAllListeners();
        this.root = root;
        this.socket = socket;
        this.users = users;
        race=raceNumber;

        this.del_text = '';
        this.list = document.getElementById('users');
        this.wrong = 0;
        this.start = false;

        document.body.addEventListener('keypress', (ev) => this.keyListener((ev)));

        this.users.forEach(user => this.addUser(user, this.list));

        this.root.style.display = "block";

        this.text_wrp = document.getElementById('text');
        this.del_wrp = document.getElementById('del');

        const token = localStorage.getItem('jwt');
        fetch('/text', {
            method: 'POST',
            headers: {
                'Authorization': `JWT ${token}`,
                'Content-Type':"application/json"
            },
            body:JSON.stringify({race})
        })
            .then(data => data.json())
            .then(data => {
                this.addText(data, this.text_wrp);
                this.current_text = data;
                this.size = this.current_text.length;
            })
            .catch(e => document.write(e.message));
        this.addEventToSocket();
    }

    addEventToSocket() {
        this.socket.on('endRace', ({raceNumber}) => {
            if(raceNumber !== race)
                return;
            this.socket.removeAllListeners();
            document.getElementById('forText').hidden = true;
            document.getElementById('time-wrp').hidden = true;
            const btn = document.getElementById('btn');
            btn.addEventListener('click', () => location.replace('/game'));
            btn.hidden = false;
        });
        this.socket.on('time_preStart', this.setTime);
        this.socket.on('start', ({raceNumber}) => {
            if(raceNumber !== race)
                return;
            this.socket.off('time_preStart', this.setTime);
            this.start = true;
            this.socket.on('time_race', this.setTime);
            this.socket.on('finish', ({raceNumber}) => {
                if(raceNumber !== race)
                    return;
                this.addText('time is up', document.getElementById('time-wrp'));
                this.start = false;
            })
        });
        this.socket.on('message', ({message, raceNumber}) =>{
            if(raceNumber!== race)
                return;
            this.log(message);
        })
    }

    addUser(user, list) {
        const user_elem = document.getElementById('forClone').cloneNode(true);
        user_elem.removeAttribute('id');
        user_elem.style.order = "0";
        user_elem.getElementsByClassName('user-name')[0].innerText = user;
        user_elem.style.display = 'block';
        list.append(user_elem);

        const correct = user_elem.getElementsByClassName('bg-success')[0];
        const incorrect = user_elem.getElementsByClassName('bg-danger')[0];
        const progress = user_elem.getElementsByClassName('progress')[0];

        let finish = false;

        let Amount =0, Wrong=0;
        this.socket.on('progress', ({amount, user: name, wrong, time}) => {
            if (!this.start || finish)
                return;
            if (amount + wrong === this.size)
                this.socket.emit('endRace');
            if (this.size === 0 || user !== name)
                return;

            Amount = amount;
            Wrong = wrong;

            const perCorrect = (amount / this.size * 100).toFixed(2);
            const perInCorrect = (wrong / this.size * 100).toFixed(2);

            user_elem.style.order = "" + (-time * (+amount + +wrong));
            correct.style.width = `${perCorrect}%`;
            incorrect.style.width = `${perInCorrect}%`;

            if (+perCorrect + +perInCorrect === 100) {
                this.socket.emit('userEndRace', ({name, amount, wrong, time}));
                finish = true;
            }
        });

        this.socket.on('userEndRace', ({name, amount, wrong, time}) => {
            if (user !== name)
                return;
            progress.hidden = true;
            user_elem.getElementsByClassName('user-statistic')[0].innerText = ` right char: ${amount}, wrong char: ${wrong}, used time: ${Race.timeConversion(+time)}`;
            user_elem.getElementsByClassName('user-status')[0].innerText = 'done';
            finish = true;
        });

        this.socket.on('end_time',({default_time,raceNumber}) =>{

            console.log('end_time');
            if(!finish && raceNumber === race){
                progress.hidden = true;
                user_elem.getElementsByClassName('user-statistic')[0].innerText = ` right char: ${Amount}, wrong char: ${Wrong}, used time: ${Race.timeConversion(+default_time)}`;
                user_elem.getElementsByClassName('user-status')[0].innerText = 'done';
                finish = true;
            }
        });
        this.socket.on('leaveUser', ({username}) => {
            if (user !== username || finish)
                return;
            progress.hidden = true;
            user_elem.getElementsByClassName('user-status')[0].innerText = 'leave';
            finish = true;
        })
    }

    addText(text, wrp) {
        wrp.innerHTML = text;
    }

    keyListener(event) {
        if (!this.start || !this.current_text || this.del_text.length > this.size)
            return;
        const key = this.current_text[0];
        if (key !== event.key)
            this.wrong++;
        this.current_text = this.current_text.slice(1);
        this.del_text += key;

        this.addText(this.current_text, this.text_wrp);
        this.addText(this.del_text, this.del_wrp);

        this.socket.emit('progress', {
            time: timeNow,
            amount: this.del_text.length - this.wrong,
            raceNumber: race,
            wrong: this.wrong
        });
    }

    log(...text) {
        if(!this.textArea)
            this.textArea = document.getElementById('textarea');
        let s = ''
        text.forEach(elem => {
            s += `${elem} \n`;
        });
        this.textArea.value += s;
        this.textArea.scrollTop = this.textArea.scrollHeight;
    }
    setTime({time, message, timeForRace, raceNumber}) {
        if(raceNumber !== race)
            return;

        if (!this.time_wrp) {
            this.time_wrp = document.getElementById('time-wrp');
        }
        if (timeForRace)
            timeNow = (timeForRace - time);

        let converted_time = Race.timeConversion(time);
        if (message)
            converted_time = `${message} ${converted_time}`;
        this.time_wrp.innerText = converted_time;
    }

    static timeConversion(milliseconds) {
        //Get hours from milliseconds
        const hours = milliseconds / (1000 * 60 * 60);
        const absoluteHours = Math.floor(hours);
        const h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

        //Get remainder from hours and convert to minutes
        const minutes = (hours - absoluteHours) * 60;
        const absoluteMinutes = Math.floor(minutes);
        const m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

        //Get remainder from minutes and convert to seconds
        const seconds = (minutes - absoluteMinutes) * 60;
        const absoluteSeconds = Math.floor(seconds);
        const s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;


        return h + ':' + m + ':' + s;
    }
}

export default Race;