import Race from './Race'

class preRace {
    constructor(root, socket, token) {
        this.root = root;
        this.socket = socket;
        this.token = token;
        this.users = new Set();

        fetch('/usersAwait', {
            method: 'GET',
            headers: {
                'Authorization': `JWT ${token}`,
            }
        })
            .then(data => data.json())
            .catch(e => document.write(e.message))
            .then(data => this.drowPanel([...data]))
            .catch(e => console.dir(e));
        this.addEventToSocket();
    }

    addEventToSocket() {
        this.socket.on('joinUser', (data) => this.appendUser(document.getElementById('users-list'), data.user));
        this.socket.once('nearlyStart', (data) => {

            document.getElementById('preRace').hidden = true;

            this.socket.emit('join_race', {room: data.raceNumber, users:data.users});
            this.socket.on('join_race', () => {
                console.log('join');
                this.socket.off('time_preRace',this.setTime);
                this.setTime({time:0});
                new Race(document.getElementById('race'), this.socket, data.users, data.raceNumber);
            });
        });
        this.socket.on('time_preRace',this.setTime);
    }

    drowPanel(users) {
        const users_list = document.getElementById('users-list');
        users.forEach(user => this.appendUser(users_list, user));
        this.root.style.display = "block";
    }

    appendUser(list, user) {
        if (this.users.has(user))
            return;

        this.users.add(user);
        const li = document.createElement('li');
        li.innerText = user;
        list.append(li);
        this.amountOfUsers();

        this.socket.on('leaveUser', (leave_user) => {
            if (user !== leave_user.user)
                return;
            this.users.delete(user);
            li.style.display = "none";
            this.amountOfUsers();
        });
    }

    amountOfUsers() {
        const amount = document.getElementById('amountOfUsers');
        amount.innerText = this.users.size;
    }


    setTime({time, message}) {
        if (!this.time_wrp) {
            this.time_wrp = document.getElementById('time-wrp');
        }
        let converted_time = Race.timeConversion(time);
        if(message)
            converted_time = `${message} ${converted_time}`;
        this.time_wrp.innerText = converted_time;
    }

    static timeConversion(milliseconds) {
        //Get hours from milliseconds
        const hours = milliseconds / (1000 * 60 * 60);
        const absoluteHours = Math.floor(hours);
        const h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

        //Get remainder from hours and convert to minutes
        const minutes = (hours - absoluteHours) * 60;
        const absoluteMinutes = Math.floor(minutes);
        const m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

        //Get remainder from minutes and convert to seconds
        const seconds = (minutes - absoluteMinutes) * 60;
        const absoluteSeconds = Math.floor(seconds);
        const s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;


        return h + ':' + m + ':' + s;
    }
}

export default preRace;