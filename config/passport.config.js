const passport = require('passport');
const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const users = require('../data/users');

const opts ={
    secretOrKey: "secretWord",
    jwtFromRequest:ExtractJwt.fromAuthHeaderWithScheme('jwt')
};

passport.use(new JWTStrategy(opts, async (payload, done) => {
    try {
        const user = users.find(userFromDB => {
            if (userFromDB.login === payload.login) {
                return userFromDB;
            }
        });
        return user ? done(null, user) : done({status: 401, message: "Token is invalid"});
    } catch(err) {
        return done(err);
    }
}));