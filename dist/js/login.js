window.onload = () => {
    if(localStorage.getItem('jwt')){
        location.replace('/game');
    }

    const btn = document.getElementById("submit");
    const login = document.getElementById("login");
    const password = document.getElementById("password");

    btn.addEventListener('click', () => {
        fetch(`/login`, {
            method: "POST",
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({login:login.value, password: password.value})
        })
            .then(data => data.json())
            .then(data => {
                if(data && data.auth){
                    localStorage.setItem('jwt', data.token);
                    location.replace('/game');
                }
                else
                    console.log("auth failed");
            })
            .catch(err => console.log(err));
    })
};