const path = require('path');

module.exports = {
    entry: __dirname + '/src/index.js',
    module: {
        rules: [{
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: [{
                    loader: "babel-loader",
                    options: {
                        configFile: "./babel.config.js",
                        cacheDirectory: true
                    }
                }]
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    mode: 'development',
    devServer: {
        inline: true
    },
    devtool: "source-map"
};