module.exports = class Storage{
    constructor(data){
        this.data = data;
        this.comments = data.comments
        this.count = 0;
    }
    getInitText(){
        return this.data.initText || '';
    }
    getStartText(){
        return this.data.startText || ''
    }
    getFinishText(){
        return this.data.finishText || '';
    }
    getComment(){
        const res =  this.comments ? this.comments[this.count%this.comments.length] : '';
        this.count++;
        return res;
    }
}