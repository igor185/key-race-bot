module.exports = class EventObserver {
    constructor() {
        this.observers = [];
    }

    subscribe(fn) {
        if (typeof fn === 'function')
            this.observers.push(fn);
    }

    unsubscribe(fn) {
        this.observers = this.observers.filter(subscriber => subscriber !== fn);
    }

    broadcast(data) {
        this.observers.forEach(fn => fn(data));
    }
}