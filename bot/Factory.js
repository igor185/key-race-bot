const Storage = require('./Storage/index');
const textForBot = require('../data/forBot');
const User = require('./User/index');
const Mailer = require('./Mailer/index');
const Statistic = require('./Statistic/index');
const Observer = require('./EventObserver/index');
const proxyHandler = require('./proxyHandler');

module.exports = class Factory {
    // factory patter
    create(send, users, sizeText) {

        let observer = new Observer();
        // observer pattern

        const storage = new Storage(textForBot);

        users = users.map(user => new User(user));

        const mailer = new Mailer(send);

        const statistic = new Statistic(users, sizeText, observer);

        const initRace = () => {
            mailer.send(storage.getInitText());
            mailer.send(statistic.listOfUsers());
        };
        const startRace = () => {
            mailer.send(storage.getStartText());
        };
        const sendStatistic = () => {
            mailer.send(statistic.toString());
        };
        const finishRace = () => {
            mailer.send(storage.getFinishText());
            mailer.send(statistic.listOfUsers(3));
        };
        const setStatistic = (data) => {
            statistic.setAbountUser(data);
        };
        const sendComment = () =>{
            mailer.send(storage.getComment());
        };

        const upToThirtySymbols = (data) => {
            mailer.send(data);
        }
        observer.subscribe(upToThirtySymbols);

        return new Proxy({
            initRace, startRace, finishRace, sendStatistic, setStatistic,sendComment
        },proxyHandler);
        //facade patter
    };
}