const _ = require('lodash');

module.exports = class Statistic {
    constructor(users, sizeText, Observer) {
        this.users = users;
        this.sizeText = sizeText;
        this.Observer = Observer;
    }

    setAbountUser(data) {
        const user = _.find(this.users, (user) => user.name === data.user);
        user.setAmount(data.amount);
        user.setWrong(data.wrong);
        user.setTime(data.time);
        this.users = this.users.sort((user1, user2) => user1.comparaTo(user2) <= 0);
        if(this.sizeText - (user.getAmount()+user.getWrong()) === 30) {
            this.Observer.broadcast(`User ${user.name} left 30 symbols`);
        }
    }

    listOfUsers(amount = this.users.length){
        let res = `List of users \n`;
        const arr = this.users.slice(0, amount);
        arr.forEach(user => res += `${user.name} \n`);
        return res;
    }
    firstThreePlayers(){

    }

    toString() {
        let res = `Rating: \n`
        this.users.forEach(user => res += `${user.toString()} \n`);
        return res;
    }
}