module.exports = {
    get(target, key){
        if(key in target)
            return target[key]
        return null;
    },
    set(target, key, value){
        console.log(`set ${key} equal ${value}`);
        throw new Error("This is forbidden action");
    },
    deleteProperty(target, key){
        console.log('try delete '+key);
        throw new Error("This is forbidden action");
    },

}