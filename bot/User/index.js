module.exports = class User{
    constructor(user){
        this.name = user;
    }
    getAmount(){
        return this.amount || 0;
    }
    setAmount(amount){
        this.amount = amount === parseInt(amount) ? amount : 0;
    }

    getWrong(){
        return this.wrong || 0;
    }
    setWrong(wrong){
        this.wrong = wrong === parseInt(wrong) ? wrong : 0;
    }

    getTime(){
        return this.time || 0;
    }
    setTime(time){
        this.time = time === parseInt(time) ? time : 0;
    }

    toString(){
        return `user ${this.name} print ${this.getAmount()} right char, ${this.getWrong()} wrong char and use ${User.timeConversion(this.getTime())}`;
    }

    comparaTo(user2){
        return (user2.getTime()*(user2.getAmount()+user2.getWrong())) - (this.getTime()*(this.getAmount()+this.getWrong()));
        // yes, it can be better
    }
    static timeConversion(milliseconds) {
        //Get hours from milliseconds
        const hours = milliseconds / (1000 * 60 * 60);
        const absoluteHours = Math.floor(hours);
        const h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

        //Get remainder from hours and convert to minutes
        const minutes = (hours - absoluteHours) * 60;
        const absoluteMinutes = Math.floor(minutes);
        const m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

        //Get remainder from minutes and convert to seconds
        const seconds = (minutes - absoluteMinutes) * 60;
        const absoluteSeconds = Math.floor(seconds);
        const s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;


        return h + ':' + m + ':' + s;
    }
}