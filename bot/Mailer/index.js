module.exports = class Mailer{
    constructor(sender, raceNumber){
        this.sender = sender;
        this.raceNumber = raceNumber
    }
    send(data){
        this.sender('message', {message: data});
    }
}